## Terminologies
1. **Docker**: A program which helps to develop and run applications with containers. It simplifies the complexity of app development.
2. **Docker Image**: An executable software that contains everything needed to run an application. It includes:
     - code
     - libraries
     - runtime
     - environment variables
     - configuration files
3. **Container**: A running docker image is a container and multiple containers can be created from it.
4. **Docker Hub**: Online platform to store docker containers.
***
## Docker Basic Commands
- To list all the running containers on the docker host: `docker ps`
- To start any stopped container: `docker start <container-id>` or `docker start <container-name>`
- To stop a running container: `docker stop <container-id>` or `docker stop <container-name>`
- To create a container from an image: `docker run <container-name>`
- To delete a container: `docker rm <container-name>`
***
## Common operations in docker
1. **Downloading a docker image/repository from dockerhub.**
     - Syntax: `docker pull [OPTIONS] NAME[:tag|@digest]`
     - If no tag is specified, `:latest` tag is considered by default.
     - Example: `docker pull debian/trydock`
2. **Running the existing docker.**
     - Syntax: `docker run [OPTIONS] IMAGE[:tag|@digest] [COMMAND] [ARG...]`  
     - Each running container is assigned a conatiner-id.  
     - Example: `docker run debian/trydock`  
3. **Accessing docker terminal.**
     - Syntax 1: `docker exec -it <container-name> /bin/bash`
          - Starts a "bash" (terminal) inside an already running container.
     - Syntax 2: `docker run -it <container-name> /bin/bash`
          - Runs a container and starts a terminal inside it.
     - To quit a docker terminal, type `exit`
4. **Copying a file inside the docker container.**
     - Syntax: `docker cp <file-name> <container-id>:/`
     - Make sure you are not inside the docker terminal.
     - Example: `docker cp hello.py e08G4RRT55KW3:/`
5. **Installing dependencies.**
     - For installing dependencies, we first have to write a script file requirements.sh
          - `apt update`
          - `apt install python3`
     - Now we need to copy this file into our container.
          - `docker cp requirements.sh e08G4RRT55KW3:/`
          - The shell script contains installation steps.
     - Give permission to run shell script.
          - `docker exec -it e08G4RRT55KW3 chmod +x requirements.sh`
     - Install dependencies.
          - `docker exec -it e08G4RRT55KW3 /bin/bash ./requirements.sh`
6. **Running the file inside the container.**
     - `docker start e08G4RRT55KW3`
     - `docker exec e08G4RRT55KW3 python3 hello.py`
7. **Saving our copied program into docker image by docker commit.**
     - Syntax: `docker commit <container-id> <image-name>`
     - Example: `docker commit e08G4RRT55KW3 debian/trydock`
8. **Pushing docker image to dockerhub.**
     - First we have to tag the image name with a different name.
          - Syntax: `docker tag <image-name> username/repo`
               - Here username is the username on dockerhub and repo is the image name.
          - Example: `docker tag debian/trydock faisal/test-image`
     - Then we upload it on dockerhub.
          - `docker push faisal/test-image`

