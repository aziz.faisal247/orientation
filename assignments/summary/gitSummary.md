## Git Basics
Git is a version-control software that enables us to collaborate over the web on a project and keep a record of the developmental stages.
***
## Terminologies
- **Repository:** It is a common space where everything related to a project is stored by every team member.
- **Commit:** It is a method to save our work in our local repository.
- **Push:** It is basically uploading our local repository onto internet.
- **Branch:** Suppose we are working on a system having many subsystems, we create a branch to work specifcally on a particular subsystem.
- **Merge:** When a *Branch* is free of errors and bugs, we merge it with the main project.
- **Clone:** Cloning is making an exact copy of a GitLab account in our local system.
- **Fork:** Forking is basically copying only the repository under our own username.
***
##  Git Internals
Suppose we are working on a file in a repository. There are three states which a file can have:
- **Modified:** It means that some change is made in that file, but it is not committed to the repository yet.
- **Staged:** The file is marked internally so that when a commit is made, all the staged (marked) files are committed together.
- **Committed:** The file is saved in the local repository.

There are 4 trees into which a file can reside:
- **Workspace:** A tree where a file is modified.
- **Staging:** A tree where all the files marked for committing are stored.
- **Local Repository:** A tree in our local system where all the committed files are stored.
- **Remote Repository:** A tree on the internet where all the local repositories from diffeent individuals are stored.
***
## Git Workflow
1. Cloning the repository
```sh
$ git clone <link-to-repository>
```
2. Creating a branch.
```sh
$ git checkout master
$ git checkout -b <branch-name>
```
3. Modifying files and adding them to the staging area.
```sh
$ git add . # '.' marks all the untracked files.
```
4. Committing the staged files to the local repository
```sh
$ git commit -sv 
```
5. Uploading the local repository to our remote repository.
```sh
$ git push origin <branch-name>
```